#ici sont mes alias personels.
#dans .bashrc, a la lignes 107, il est ecrit que je peux ajouter mes alias ici.
#Le fichier .bashrc fera appel a ce fichier.

#Ma première odification de $PS1 était PS1="\[\033[1;32m\]""${PS1}""\[\033[00m\]"
#Modification $PS1
PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u\[\033[00m\]|\[\033[01;32m\]\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
#Modification $PS2
PS2="\[\033[1;31m\]>\[\033[00m\]"

#constantes de couleur:
NORMAL="\\033[0;39m"
VIOLET="\\033[1;35m"
JAUNE="\\033[1;33m"
CYAN="\\033[1;36m"
BLANC="\\033[0;37m"
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
GRAS="\\033[1;39m"

#en attendant ...
PATH="$PATH:/opt/java/jdk1.8.0_45/bin"

#alias défini dans mon .bashrc mais pas à l'unif
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

#divers alias
alias rmwave='echo -e "$ROUGE" "Supression de tous les fichiers se terminant par ~ a partir d ici";echo -ne "$NORMAL";find -name "*~" -exec /bin/rm {} \;'
alias rmiwave='echo -e "$ROUGE" "Supression de tous les fichiers se terminant par ~ a partir d ici";echo -ne "$NORMAL";find -name "*~" -exec /bin/rm -i {} \;'
alias admin='echo "Execution en mode superUtilisateur.";sudo'
alias s='echo -ne "$JAUNE" "Execution en mode superutilisateur.";echo -e "$NORMAL";sudo'
alias rmi='rm -i'
alias cd..='cd ..'
alias raccourci='gnome-desktop-item-edit --create-new ~/Bureau'

#alias pour utiliser mes petites commandes bash dans .jasonComm
alias testdir='. ~/.jasonComm/testdir'
alias godir='. ~/.jasonComm/godir'
alias addgitignore='cp ~/.jasonComm/.gitignore .'
alias cadre='. ~/.jasonComm/cadre'
alias jc='~/.jasonComm/jc'

#Pour changer de disposition clavier
alias resetkeyboard='echo -ne "$JAUNE" "Retour au clavier belge.";echo -e "$NORMAL";setxkbmap be'
alias bekeyboard='echo "Retour au clavier belge.";setxkbmap be'
alias uskeyboard='echo -ne "$ROUGE" "ATTENTION: claver qwerty.";echo -e "$NORMAL";setxkbmap us'
alias frkeyboard='echo -ne "$ROUGE" "ATTENTION: clavier français.";echo -e "$NORMAL";setxkbmap fr'
